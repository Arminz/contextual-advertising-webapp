import scrapy

suitable_headers = {
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.9,fa;q=0.8',
                    'Accept-Encoding': 'zip, deflate, br',
                    'Host': 'www.digikala.com',
                    'Referer': 'https://www.digikala.com/',
                    'Upgrade-insecure-request': '1',
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
                    'Connection': 'keep-alive'}
def clear_text(text):
    return text\
        .strip()\
        .replace('\n', ' ')\
        .replace('\r', ' ')

class BrickSetSpider(scrapy.Spider):
    name = "brickset_spider"
    # start_urls = ['https://www.digikala.com/main/personal-appliance/',
    #              'https://www.digikala.com/main/vehicles/',
    #              'https://www.digikala.com/main/apparel/',
    #              'https://www.digikala.com/main/home-and-kitchen/',
    #              'https://www.digikala.com/main/book-and-media/',
    #              'https://www.digikala.com/main/mother-and-child/',
    #              'https://www.digikala.com/main/sport-entertainment/',
    #              'https://www.digikala.com/main/food-beverage/']

    start_urls = ['https://www.digikala.com/']

    def start_requests(self):
        for url in self.start_urls:
            print ('start_url : {}'.format(url))
            request = scrapy.Request(url,
                                     headers=suitable_headers,
                                     callback=self.open_page_sections)
            yield request

    def open_page_sections(self, response):
        SECTION_SELECTOR = ".c-navi-new__big-display-title"
        ad_sections = response.css(SECTION_SELECTOR)
        log_url = 'number of ad sections: {}\n'.format(len(ad_sections))
        print (log_url)
        with open('./digikala-log-t.csv', 'a') as fl:
            fl.write(log_url)
        for ad_section_title in ad_sections:
            LINK_SELECTOR = 'a::attr(href)'
            section_page_url = ad_section_title.css(LINK_SELECTOR).extract_first()
            yield scrapy.Request(
                response.urljoin(section_page_url),
                headers=suitable_headers,
                callback=self.open_ad_page
            )

    

    def open_ad_page(self, response):
        ADD_SELECTOR = '.c-product-box'
        ad_list = response.css(ADD_SELECTOR)
        log_url = 'number of ad boxes for url {} : {}\n'.format(response.request.url, len(ad_list))
        print (log_url)
        with open('./digikala-log-t.csv', 'a') as fl:
            fl.write(log_url)
        for ad in ad_list:
            NAME_SELECTOR = 'a::attr(href)'
            next_page = ad.css(NAME_SELECTOR).extract_first()
            yield scrapy.Request(
                response.urljoin(next_page),
                headers=suitable_headers,
                callback=self.crawl_ad_page
            )

    def crawl_ad_page(self, response):
        print('crawling the ad: {}\n'.format(response.request.url))
        TEXT_SELECTOR = '.c-mask__text::text'
        TITLE_SELECTOR = '.c-product__title::text'
        CATEGORY_SELECTOR = '.c-product__directory'
        text = response.css(TEXT_SELECTOR).extract_first()
        title = clear_text(response.css(TITLE_SELECTOR).extract_first())
        url = response.request.url
        text = '' if (text == None) else clear_text(text)
        title = '' if (title == None) else clear_text(title)
        
        # category = response.css(CATEGORY_SELECTOR).extract_first()
        with open('../digikala-data-t.csv', 'a') as fd:
            fd.write("{},{},{}\n".format(url, title, text))

        yield {
            'title': title,
            # 'category': category.strip('\n'),
            'text': text
        }