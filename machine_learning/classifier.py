#%%

from sklearn.utils import resample
import pandas as pd
import numpy as np

from config_yektanet import *

from report.utils import plot_distribution

#%%
df = pd.read_csv(DATA_ADDRESS + 'web-transformed.csv')
df.head()
#%%
df['category'].value_counts()
#%%
plot_distribution(df, 'category', 'web_dist.png')


#%%
df_majority = df[(df['category'] == 'اخبار ورزشی')]
df_majority_2 = df[df['category'] == 'خبرهای سیاسی و اجتماعی، جامعه و فرهنگ']
df_others = \
    df[(df['category'] != 'اخبار ورزشی') & (df['category'] != 'خبرهای سیاسی و اجتماعی، جامعه و فرهنگ')]

df_majority_downsample = resample(df_majority, replace=False, n_samples=600)
df_majority_downsample_2 = resample(df_majority_2, replace=False, n_samples=600)

df = pd.concat([df_others, df_majority_downsample, df_majority_downsample_2])
df['category'].value_counts()

#%%
plot_distribution(df, 'category', 'web_resampled_dist.png')

#%%

# from sklearn.ensemble import RandomForestClassifier
# model = RandomForestClassifier(n_estimators=10)

from sklearn.naive_bayes import GaussianNB
model = GaussianNB() #11%!

# from sklearn.tree import DecisionTreeClassifier
# model = DecisionTreeClassifier()

# from sklearn.ensemble import ExtraTreesClassifier
# model = ExtraTreesClassifier() 

# from sklearn.linear_model import LogisticRegression
# model = LogisticRegression()

labels_train = df['category'].iloc[:int(0.8*len(df))]
labels_train.fillna('others', inplace=True)
train = df.drop(['category', 'url'], axis=1).iloc[:int(0.8*len(df))]

labels_test = df['category'].iloc[int(0.8*len(df)):]
test = df.drop(['category', 'url'], axis=1).iloc[int(0.8*len(df)):]
model.fit(train, labels_train)

#%%
accuracy = (model.predict(test) == labels_test).sum() / len(labels_test)
print('accuracy: {}'.format(accuracy))

#%%
ads = pd.read_csv(DATA_ADDRESS + 'ads-transformed.csv', index_col=0)

#%%
ads['category'] = model.predict(ads)
ads['category'].value_counts()
#%%
plot_distribution(ads, 'category', 'ads_dist_{}.png'.format(type(model).__name__))

#%%
categories_proba = model.predict_proba(ads.drop('category', axis=1))

#%%
for category in model.classes_:
    ads[category] = categories_proba[:,model.classes_.tolist().index(category)]
#%%
ads.head()
#%%
ads[['category'] + model.classes_.tolist()].to_csv(DATA_ADDRESS + 'ads-labeled.csv')

#%%

from sklearn.externals import joblib

joblib.dump(model, CLASSIFIER_ADDRESS)






#%%
