#%%
import pandas as pd
from sklearn.externals import joblib
from config_yektanet import *


#%%
transformer = joblib.load(TRANSFORMER_ADDRESS)

#%%

ads = pd.read_csv(DATA_ADDRESS + 'digikala-data.csv',header=None, error_bad_lines=False)
ads = ads[~ads[2].isna()]
ads.head()


#%%

from machine_learning.utils import *
transformed_ads, transformer = extract_text_features(ads[2], transformer)
transformed_ads.index = ads[0]
#%%
transformed_ads.to_csv(DATA_ADDRESS + 'ads-transformed.csv')



