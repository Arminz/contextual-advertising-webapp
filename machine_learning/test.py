#%%
import pandas as pd
from config_yektanet import *


#%%
data = pd.read_csv(DATA_ADDRESS + 'digikala-data.csv', header=None, error_bad_lines=False)


#%%
data.head()


#%%
data[0]

#%%
1 - (data[2].isna().sum()/ data[2].isna().count())

#%%
data[~data[2].isna()].count()

#%%

ads = pd.read_csv(DATA_ADDRESS + 'ads-labeled.csv')
ads.head()
#%%
ads.iloc[:,1].value_counts()

#%%

#%%
web = pd.read_csv(DATA_ADDRESS + 'web-transformed.csv')
web.head()

#%%
web.iloc[:,-1].value_counts()

#%%



#%%
classified_ads = pd.read_csv(DATA_ADDRESS + 'ads-labeled.csv')
classified_ads.iloc[:,1].value_counts()


#%%
from report.utils import plot_distribution
plot_distribution(classified_ads, 'category', 'ads_distribution.png')

#%%
