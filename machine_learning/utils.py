import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer


def extract_text_features(np_array, transformer=None):
    if transformer == None:
        transformer = TfidfVectorizer(min_df=0., max_df=1.)
        matrix = transformer.fit_transform(np_array)
    else:   
        matrix = transformer.transform(np_array)

    matrix = matrix.toarray()
    trasformed_df = pd.DataFrame(matrix, columns = transformer.get_feature_names())
    return trasformed_df, transformer

