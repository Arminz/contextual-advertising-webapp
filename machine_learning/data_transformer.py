#%%
import pandas as pd
import numpy as np
from sklearn.externals import joblib
from config_yektanet import *

import json

#%%
web_data = pd.read_csv(DATA_ADDRESS + 'web.csv', header=None)
web_data.head()


#%%

text_df = pd.DataFrame({'text' : web_data.iloc[:,4]})
text_df.head()


#%%
# import nltk
# import re

# wpt = nltk.WordPunctTokenizer()
# # stop_words = nltk.corpus.stopwords.words('english')

# def normalize_document(doc):
#     # lower case and remove special characters\whitespaces
#     # doc = re.sub(r'[^a-zA-Z\s]', '', doc, re.I|re.A)
#     # doc = doc.lower()
#     # doc = doc.strip()
#     # tokenize document
#     tokens = wpt.tokenize(doc)
#     # filter stopwords out of document
#     # filtered_tokens = [token for token in tokens if token not in stop_words]
#     # re-create document from filtered tokens
#     doc = ' '.join(tokens)

#     return doc

# normalize_document = np.vectorize(normalize_document)
#TODO: use this

#%%
import hazm

def normalize_document(document):
	if (type(document) == float):
		return 'Empty'
	hwt = hazm.WordTokenizer()
	tokens = hwt.tokenize(document)

	filtered_tokens = [token for token in tokens 
		if token not in hazm.stopwords_list()]

	return ' '.join(filtered_tokens)

normalize_document = np.vectorize(normalize_document)
#%%


normalled_corpus = normalize_document(text_df['text'].iloc[0:100])
#%%


from machine_learning.utils import extract_text_features
transformed_df, transformer = extract_text_features(text_df['text'][~text_df['text'].isna()])
transformed_df.head()


#%%
joblib.dump(transformer, TRANSFORMER_ADDRESS)

#%%
import json
def extract_category(category_json_string):
    category_json = json.loads(category_json_string)
    category_list = []
    probability_list = []
    for category in category_json:
        category_list.append(category_json[category]['category_title'])
        probability_list.append(category_json[category]['probability'])
    if not category_list:
        return None
    pi = probability_list.index(max(probability_list))
    return(category_list[pi])


#%%
transformed_df['category'] = web_data.iloc[:,14].map(extract_category)
transformed_df['category'].unique()

#%%


#%%

transformed_df.to_csv(DATA_ADDRESS + 'web-transformed.csv')




#%%
