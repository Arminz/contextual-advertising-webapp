from rest_framework import serializers

from ads.models import Page
from django.db import models


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ['created', 'title', 'content', 'url', 'category']
