from django.http import JsonResponse
from rest_framework import status, generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from ads.models import Page
from ads.serializers import PageSerializer
from sklearn.externals import joblib
import numpy as np
import pandas as pd

from machine_learning.utils import extract_text_features

from django.views.decorators.csrf import csrf_exempt
from config_yektanet import *

model = joblib.load(CLASSIFIER_ADDRESS)
transformer = joblib.load(TRANSFORMER_ADDRESS)

class PageDetail(generics.RetrieveAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer


class PageList(generics.ListCreateAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def perform_create(self, serializer):
        df, _ = extract_text_features(np.array([serializer.validated_data['content']]), transformer)
        prediction = model.predict(df)
        category = prediction[0]
        serializer.save(category=category)

@csrf_exempt
def ads_recommendation(request, page_url):
    if request.method == 'GET':
        page_list = Page.objects.filter(url = page_url)
        if len(page_list) == 0:
            return JsonResponse({}, status=404)
        page = page_list[0]
        category = page.category
        ads_df = pd.read_csv(DATA_ADDRESS + 'ads-labeled.csv', index_col=0)
        suitable_ads = ads_df\
            .sort_values(by=category, ascending=False)[category][0:10]
        return JsonResponse(suitable_ads.to_dict())
    

