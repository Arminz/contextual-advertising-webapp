from django.db import models

class Page(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.TextField()
    content = models.TextField()
    url = models.TextField()
    category = models.TextField(blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['created']
