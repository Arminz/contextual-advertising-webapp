from django.contrib import admin

# Register your models here.
from ads.models import Page

admin.site.register(Page)