from matplotlib import pyplot as plt
import seaborn as sns
from config_yektanet import REPORT_ADDRESS

def plot_distribution(data, column, title):
    fig = plt.figure(figsize=(16,6))
    ax = sns.countplot(x=column, 
        data = data,order=data[column].value_counts().index)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
    fig.savefig(REPORT_ADDRESS + title)