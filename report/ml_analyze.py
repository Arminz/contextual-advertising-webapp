#%%

import pandas as pd
from config_yektanet import *
from report.utils import plot_distribution
#%%
classified_ads = pd.read_csv(DATA_ADDRESS + 'ads-labeled.csv')
classified_ads.iloc[:,1].value_counts()
#%%
plot_distribution(classified_ads, 'category', 'ads_dist.png')


#%%
