DATA_ADDRESS = './machine_learning/data/'
PERSISTED_MODELS_ADDRESS = './machine_learning/persisted_models/'
REPORT_ADDRESS = './report/'

CLASSIFIER_ADDRESS = PERSISTED_MODELS_ADDRESS + 'classifier.pk'
TRANSFORMER_ADDRESS = PERSISTED_MODELS_ADDRESS + 'transformer.pk'


